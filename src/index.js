import SortableColumns from './SortableColumns';
import VueI18n from 'vue-i18n'
// Declare install function executed by Vue.use()
export function install(Vue) {
    if (install.installed) {
        return;
    }
    install.installed = true;
    Vue.use(VueI18n);
    Vue.component('sortable-columns', SortableColumns);
}


const SortableColumnsPlugin = {
    install
};
let SortableColumnsVue = null;
if (typeof window !== 'undefined') {
    SortableColumnsVue = window.Vue;
} else if (typeof global !== 'undefined') {
    SortableColumnsVue = global.Vue;
}
if (SortableColumnsVue) {
    SortableColumnsVue.use(SortableColumnsPlugin);
}
// To allow use as module (npm/webpack/etc.) export component
export default SortableColumns;
