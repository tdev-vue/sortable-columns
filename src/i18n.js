import Vue from "vue";
import VueI18n from 'vue-i18n'
import ru from './i18n/ru'
import de from "./i18n/de"
import en from "./i18n/en"

Vue.use(VueI18n);
const lang = document.querySelector('html').getAttribute('lang');
export default new VueI18n({
  locale: lang || 'de',
  fallbackLocale: lang,
  messages: {
    ru,
    de,
    en
  }
})
