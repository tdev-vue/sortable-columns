export default {

  search: "Search",
  show: "Show",
  entities: "Entities",
  total: "Show {from} to {to} of {total} ({last_page} pages in total)"
}
