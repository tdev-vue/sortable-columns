export default {
    search: "Suche",
    show: "Anzeigen",
    entities: "Entitäten",
    total: "Zeige {from} bis {to} von {total} ({last_page}  Seiten insgesamt)"
}
