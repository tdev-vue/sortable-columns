export default {
    search: "Поиск",
    show: "Показать",
    entities: "элементов",
    total: "Показано с {from} по {to} из {total} (всего {last_page} страниц)"
}